import React,{useEffect, useState} from 'react';
import {
  Text,
  View,
  Image,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
// import styles from './styles';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {SCREEN_NAME} from '../navigation';

export default props => {

  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  const login = () => {
    props.navigation.navigate(SCREEN_NAME.LOGIN, {
      user: username,
      pass: password,
    });
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.userInfoSection}>
        <View style={{flexDirection: 'row',marginTop: 15}}>
          <Image
            style={styles.iconprofile}
            source={{
              uri:
                'https://icons-for-free.com/iconfiles/png/512/business+costume+male+man+office+user+icon-1320196264882354682.png',
            }}
          />
          <View style={{marginLeft: 20}}>
            <Text style={styles.title}>{props.route?.params?.user}</Text>
            {/* <Text style={styles.caption}>@Sophearin123</Text> */}
          </View>
        </View>
      </View>

      <View style={styles.userInfoSection}>
        <View style={styles.row}>
        <FontAwesome5 name={'address-book'} size={20} color="red" />
          <Text style={styles.titleaddress}>Phnom Penh, Cambodia</Text>
        </View>
        <View style={styles.row}>
          <FontAwesome5 name={'phone-square'} size={20} color="red" />
          <Text style={styles.titleaddress}>093 222 715</Text>
        </View>
        <View style={styles.row}>
          <FontAwesome5 name={'envelope'} size={20} color="red" />
          <Text style={styles.titleaddress}>test0@gmail.com</Text>
        </View>
      </View>
      <View style={styles.menuWrapper}>
        <TouchableOpacity style={styles.menuItem}>
        <FontAwesome5 name={'history'} size={30} color="red" />
          <Text style={styles.menuItemText}>Experiences</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.menuWrapper}>
        <TouchableOpacity style={styles.menuItem}>
        <FontAwesome5 name={'chart-line'} size={30} color="red" />
          <Text style={styles.menuItemText}>Level</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.menuWrapper}>
        <TouchableOpacity style={styles.menuItem}>
        <FontAwesome5 name={'graduation-cap'} size={30} color="red" />
          <Text style={styles.menuItemText}>Education</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.menuWrapper}>
        <TouchableOpacity style={styles.menuItem}>
        <FontAwesome5 name={'user-cog'} size={30} color="red" />
          <Text style={styles.menuItemText}>Settings</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.menuWrapper}>
        <TouchableOpacity style={styles.menuItem} onPress={login}>
        <FontAwesome5 name={'sign-out-alt'} size={30} color="red" />
          <Text style={styles.menuItemText}>Logout</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  iconprofile: {
    width: 80,
    height: 80,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    // marginLeft: 100,
    marginTop: 15,
    marginBottom: 5,
  },
  userInfoSection: {
    paddingHorizontal: 30,
    marginBottom: 25,
    marginTop: 20
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoBoxWrapper: {
    borderBottomColor: '#ddd',
    borderBottomWidth: 1,
    borderTopColor: '#ddd',
    borderTopWidth: 1,
    flexDirection: 'row',
    height: 100,
  },
  infoBox: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    // color: 'gray',
    marginTop: -3,
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
  titleaddress: {
    color: 'gray',
    marginLeft: 20,
  },
  borderbox: {
    borderRightColor: '#ddd',
    borderRightWidth: 1,
  },
});

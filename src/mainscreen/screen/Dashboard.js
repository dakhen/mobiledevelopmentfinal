import React,{useEffect, useState} from 'react';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
// import FeatherIcon from '@expo/vector-icons/Feather'
import axios from 'axios';


export default props => {
    const [dashboard, setDashboard] = useState([]);
    // const [campanies, setCompanies] = useState();
    // const [categories, setCategories] = useState();
    // const [users, setUsers] = useState();
    useEffect(() => {
      axios
        .get('http://192.168.0.9:8080/api/dashboard')
      .then(response => {
          console.log(JSON.stringify(response.data.Data));
          setDashboard(response.data.Data);
        })
          .catch((err) => {
            console.log(err);
          });
      });
  return (
    <SafeAreaView style={styles.safeAreaView}>
      <View style={styles.container}>
        <Header title="Dashboard" />
        <Card title="TOTAL JOB" iconName="heart" amount="7" />
        <Card
          title="TOTAL COMPANY"
          iconName="heart"
          amount="3"
          titleStyle={{color: 'green'}}
        />
        <Card
          title="TOTAL CATEGORY"
          iconName="heart"
          amount="5"
          titleStyle={{color: 'green'}}
        />

        <Card
          title="TOTAL USER"
          iconName="heart"
          amount="2"
          titleStyle={{color: 'orange'}}
        />
      </View>
    </SafeAreaView>
  )
};

const Header = ({title}) => {
  return (
    <View style={styles.headerWrapper}>
      <Text style={styles.headerTitle}>{title}</Text>
    </View>
  )
}

const Card = ({title, iconName, amount, titleStyle}) => {
  return (
    <View style={styles.cardWrapper}>
      <View style={styles.leftItem}>
        <Text style={[styles.title, titleStyle]}>{title}</Text>
        <Text style={styles.amount}>{amount}</Text>
      </View>
      <View style={styles.rightItem}>
        {/* <FeatherIcon name={iconName} size={25} color="black" /> */}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },

  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 16,
  },

  headerWrapper: {
    marginVertical: 16,
  },

  headerTitle: {
    fontSize: 24,
    fontWeight: 'bold',
  },

  cardWrapper: {
    padding: 16,
    flexDirection: 'row',
    backgroundColor: 'white',
    marginVertical: 8,
    borderWidth: 1,
    borderColor: 'lightgray',

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },

  title: {
    marginVertical: 8,
    fontWeight: 'bold',
    fontSize: 16,
    color: 'lightblue',
  },

  amount: {
    marginVertical: 8,
    fontSize: 16,
    fontWeight: 'bold',
  },

  leftItem: {
    flex: 1,
  },

  rightItem: {
    justifyContent: 'center',
  },
});

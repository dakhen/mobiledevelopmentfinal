import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  FlatList,
  TouchableHighlight
} from 'react-native';
import styles from './styles';
import axios from 'axios';
import {SCREEN_NAME} from '../navigation';

export default props => {

  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  const job = () => {
    props.navigation.navigate(SCREEN_NAME.JOB, {
      user: username,
      pass: password,
    });
  };
  const image = {
    uri:
      'https://lh3.googleusercontent.com/proxy/4sKpTDVEbqO9hAnAfiWYPXJblzgDiBvLHCIUs7RjkvaMtdcoanUXvN8Z7b0BTCVoZswRXsSI98IBQLBfapGkeX7md521Tz1hr3TibA0On8RWm7-Wm8tKfORDcUaS15g',
  };

  const [jobs, setJobs] = useState([]);
  useEffect(() => {
    axios
      .get('http://192.168.0.9:8080/api/status')
      .then((response) => {
        console.log(JSON.stringify(response.data.Data));
        setJobs(response.data.Data);
      })
      .catch((err) => {
        console.log(err);
      });
  });
  return (
    <View>
      <ImageBackground
        source={require('../src/opportunity.jpg')}
        style={style.image}
        imageStyle={{borderBottomLeftRadius: 30, borderBottomRightRadius: 30}}
      />
      <TouchableHighlight onPress={job} style={style.jobapplybtn}>
        <Text style={style.jobapplytxt}>Apply Job</Text>
      </TouchableHighlight>
      <View style={style.cardjob}>
        <ScrollView>
          {jobs.map((job) => (
            <Job name={job.status_title} Des={job.status_des} />
          ))}
        </ScrollView>
        {/* <Job name="Test" Des="Test"/> */}

        <Text>Title: Web Developer</Text>
        <Text style={{marginTop:10}}>Description: developer website</Text>
        <Text style={{marginTop:10}}>Requirement: php,html, css, javascript</Text>
        <Text style={{marginTop:10}}>Address: Phnom Penh</Text>
        <Text style={{marginTop:10}}>Salary: Negotiable</Text>
        <Text style={{marginTop:10}}>Job closed: 2020-12-31</Text> 
      </View>
    </View>
  );
};
// const Job = ({name, Des}) => {
//      return (
//        // eslint-disable-next-line prettier/prettier
//        <View
//          style={styles.Listjob}>
//       <Image
//         source={{
//           uri:
//             'https://www.orientenergyreview.com/wp-content/uploads/2018/12/newjobs.jpg',
//         }}
//         style={styles.imagejob}
//       />
//       <View style={styles.TextListJob}>
//         <TouchableOpacity>
//           <Text>{name}</Text>
//           <Text note style={styles.Des}>
//             {Des}
//           </Text>
//         </TouchableOpacity>
//       </View>
//     </View>
//   );
// };
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    height: 280,
    justifyContent: 'flex-end',
  },
  jobapplybtn: {
    position: 'absolute',
    right: 12,
    top: 250,
    backgroundColor: 'blue',
    padding: 16,
    borderRadius: 40,
  },
  jobapplytxt: {
    color: 'white',
    fontSize: 14,
  },
  cardjob: {
    backgroundColor: '#eee',
    marginTop: 30,
    marginLeft: 30,
    
  }
})

// export default JobDetail;
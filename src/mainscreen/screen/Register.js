import React, { useState } from 'react';
import {Text, View, StyleSheet, Image, TouchableHighlight, TextInput} from 'react-native';
import {SCREEN_NAME} from '../navigation';

export default props => {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  const register = () => {
    props.navigation.navigate(SCREEN_NAME.PROFILE, {
      user: username,
      pass: password,
    });
  };

  return (
     <View style={styles.container}>
        
     <View style={styles.logo}>
       <Image style={{ resizeMode: 'contain', width: 100 }} source={require('../src/jobpostings1.png')} />
     </View>

     <View style={styles.inputContainer}>
       <Image style={styles.inputIcon} source={require('../src/girluser1.png')} />
       <TextInput style={styles.inputs}
         placeholder="Email"
         underlineColorAndroid='transparent'
         onChangeText={setUsername} />
     </View>

     <View style={styles.inputContainer}>
       <Image style={styles.inputIcon} source={require('../src/password1.png')} />
       <TextInput style={styles.inputs}
         placeholder="Password"
         secureTextEntry={true}
         underlineColorAndroid='transparent'
         onChangeText={setPassword} />
     </View>
    
     <View style={styles.inputContainer}>
       <Image style={styles.inputIcon} source={require('../src/password1.png')} />
       <TextInput style={styles.inputs}
         placeholder="Confirm Password"
         secureTextEntry={true}
         underlineColorAndroid='transparent'
         onChangeText={setPassword} />
     </View>

     <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={register}>
       <Text style={styles.loginText}>SIGN UP</Text>
     </TouchableHighlight>

     {/* <TouchableHighlight style={styles.buttonContainer} onPress={() => this.onClickListener('restore_password')}>
       <Text>Forgot your password?</Text>
     </TouchableHighlight>
     <TouchableHighlight style={styles.buttonContainer} onPress={() => this.onClickListener('restore_password')}>
       <Text>Sign Up</Text>
     </TouchableHighlight> */}
   </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DCDCDC',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
  },
  loginButton: {
    backgroundColor: "#8eed21",
  },
  loginText: {
    color: 'white',
  },

  logo: {
    marginTop: -300,
    marginBottom: -300,
  }
});

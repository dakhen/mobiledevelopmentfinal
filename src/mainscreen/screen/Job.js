import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StatusBar,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import styles from './styles';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import 'react-native-gesture-handler';
// import {List} from 'react-native-paper';
import axios from 'axios';
import {SCREEN_NAME} from '../navigation';


const colors = {
  themecolor: '#4263ec',
  white: '#fff',
  background: '#f4f6fc',
  greyish: '#a4a4a4',
  tint: '#2b49c3',
};

export default props => {
  const [jobs, setJobs] = useState([]);
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  const jobdetail = () => {
    props.navigation.navigate(SCREEN_NAME.JOBDETAILS, {
      user: username,
      pass: password,
    });
  };


  useEffect(() => {
    // axios.get('http://job-posting-final.herokuapp.com/api/job/showall')
    // .then((result) => {
    //   console.log("test");
    //   //console.log(JSON.stringify(result.data));
    //   setJobs(result.data.Data);
    // });
    axios
      .get(' http://192.168.0.9:8080/api/job')
      .then((response) => {
        console.log(JSON.stringify(response.data.Data));
        setJobs(response.data.Data);
      })
      .catch((err) => {
        console.log(err);
      });
  });
  return (
    <View style={styles.container}>
      {/* <FontAwesome5 name={'comments'} size={100} color="red" /> */}
      {/* <Text>Hello</Text> */}
      <StatusBar barStyle="light-content" backgroundColor={colors.themecolor} />
      <View style={styles.header}>
        <FontAwesome5 name={'align-justify'} size={30} color="white" />
        <View style={{flexDirection:"row"}}>
          {/* <FontAwesome5 name={'user-circle'} size={35} color="white" /> */}
          {/* <Image
            source={{
              uri:
                'https://z-p3-scontent.fpnh5-4.fna.fbcdn.net/v/t1.0-9/36451496_1654990594619058_793084789451128832_o.jpg?_nc_cat=110&_nc_sid=174925&_nc_eui2=AeGTilT7M8FBXANDg1xoby1X0OPGaqJlZpTQ48ZqomVmlIdXYUqCrn75KT0vBAd4F7mLBu0IydmQ1o2oJoLMBF1_&_nc_ohc=isVai51u788AX9CGNdl&_nc_ht=z-p3-scontent.fpnh5-4.fna&oh=ddc5bb1ab59cb060602b4db16db3a4a2&oe=5FAECEA5'}}
            style={styles.imageprofile}
          /> */}
        </View>
      </View>
      <View style={{padding:16}}>
        <Text style={styles.textheader}>{'Job \nPosting'}</Text>
        <View style={styles.search}>
          <FontAwesome5 name={'search'} size={20} color="white"/>
          <TextInput placeholder="Search"></TextInput>
        </View>
      </View>
      <ScrollView>
        {jobs.map((job) => (
          <TouchableHighlight onPress={jobdetail}>
          <Job name={job.job_name} Des={job.job_description} />
        </TouchableHighlight>
        ))}
      </ScrollView>
    </View>
  );
};
const Job = ({name, Des}) => {
  return (
    // eslint-disable-next-line prettier/prettier
    <View
      style={styles.Listjob}>
      <Image
        source={{
          uri: 'https://www.orientenergyreview.com/wp-content/uploads/2018/12/newjobs.jpg',
        }}
        style={styles.imagejob}
      />
      <View style={styles.TextListJob}>
        <TouchableOpacity>
          <Text>{name}</Text>
          <Text note style={styles.Des}>
            {Des}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

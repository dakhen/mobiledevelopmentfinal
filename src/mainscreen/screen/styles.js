import {StyleSheet} from 'react-native';
const colors = {
  themecolor: '#4263ec',
  white: '#fff',
  background: '#f4f6fc',
  greyish: '#a4a4a4',
  tint: '#2b49c3',
};
const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: colors.themecolor,
  },
  header:{
    padding: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  search:{
    paddingHorizontal: 10,
//     paddingVertical: 6,
    flexDirection: 'row',
//     justifyContent: 'space-between',
    backgroundColor: colors.tint,
    borderRadius: 20,
    marginVertical: 10,
    alignItems: 'center',
  },
  textheader:{
    color: colors.white,
    fontSize: 35,
    textAlign: 'center',
  },
  imageprofile:{
    width: 45,
    height: 45,
    borderRadius: 25,
  },
  imagejob:{
    width: 45,
    height: 45,
    borderRadius: 25,
  },
  Listjob: {
    flexDirection: 'row',
    backgroundColor: '#eee',
    margin: 5,
    padding: 10,
  },
  TextListJob: {
    marginTop: 3,
    marginLeft: 20,
  }
});
export default styles;
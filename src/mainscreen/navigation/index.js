import React from 'react';
import {Image, View, Text, StyleSheet} from 'react-native';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {createStackNavigator, HeaderBackButton} from '@react-navigation/stack';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, } from '@react-navigation/drawer';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import Profile from '../screen/Profile';
import Login from '../screen/Login';
import Dashboard from './../screen/Dashboard';
import Job from '../screen/Job';
import Register from '../screen/Register';
import ForgetPassword from '../screen/ForgetPassword';
import JobDetail from './../screen/JobDetail';
import Company from '../screen/Company';


const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

// snake case with upper case for constant
export const SCREEN_NAME = {
  LOGIN: 'Login',
  REGISTER: 'Register',
  PROFILE: 'Profile',
  DASHBOARD: 'Dashboard',
  JOB: 'Job',
  FORGETPASS: 'ForgetPassword',
  JOBDETAILS: 'JobDetail',
  COMPANY: 'Company',
};

const OpenDraw = () => {
  const navigation = useNavigation();
  return (
    <TouchableWithoutFeedback onPress={navigation.openDrawer}>
      <View style={styles.drawIcon}>
        <Text>
          <Image style={{resizeMode: 'contain', width: 100 }} source={require('../src/menu.jpg')} />
        </Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

const StackScreen = () => {
  return (
    <Stack.Navigator
      initialRouteName={SCREEN_NAME.LOGIN}
      screenOptions={screenProps => ({
        headerTitleAlign: 'center',
        headerTintColor: 'green',
        headerTitleStyle: {
          fontSize: 10,
        },
        headerLeft: props => <OpenDraw {...props} />,
        headerRight: props => {
          if (!screenProps.navigation.canGoBack()) {
            return '';
          }
          return (
            <HeaderBackButton
              {...props}
              onPress={() => {
                screenProps.navigation.goBack();
              }}
            />
          );
        },
      })}>
      <Stack.Screen
        name={SCREEN_NAME.PROFILE}
        component={Profile}
        options={{title: 'Profile Page'}}
      />
      <Stack.Screen
        name={SCREEN_NAME.DASHBOARD}
        component={Dashboard}
        options={{
          headerStyle: {
            backgroundColor: 'black',
          },
        }}
      />
       <Stack.Screen
        name={SCREEN_NAME.REGISTER}
        component={Register}/>
      <Stack.Screen
        name={SCREEN_NAME.FORGETPASS}
        component={ForgetPassword}/>
        <Stack.Screen
        name={SCREEN_NAME.JOBDETAILS}
        component={JobDetail}/>
        <Stack.Screen
        name={SCREEN_NAME.COMPANY}
        component={Company}/>
      
      
      <Stack.Screen name={SCREEN_NAME.LOGIN} component={Login} />
    </Stack.Navigator>
  );
};

export default () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator
        initialRouteName={SCREEN_NAME.LOGIN}
        drawerContent={CustomDrawerContent}
        drawerContentOptions={{
          activeTintColor: '#84e467',
        }}>
        <Drawer.Screen name={SCREEN_NAME.DASHBOARD} component={Dashboard} />
        <Drawer.Screen name={SCREEN_NAME.JOB} component={Job} />
        <Drawer.Screen name={SCREEN_NAME.COMPANY} component={Company} />
        <Drawer.Screen name={SCREEN_NAME.LOGIN} component={StackScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};

const CustomDrawerContent = props => {
  return (
    <DrawerContentScrollView {...props}>
      <View style={{alignContent: 'center', alignItems: 'center'}}>
         <Image style={{resizeMode: 'contain', width: 100 }} source={require('../src/girluser.png')} />
      </View>
      <DrawerItemList {...props} />
    </DrawerContentScrollView>
  );
};

const styles = StyleSheet.create({
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
  },
  drawIcon: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },
});